﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using App.Core.Models.Configuration;
using App.Core.Services.Configuration;
using Microsoft.AspNetCore.Mvc;


namespace App.Controllers
{
    [Route("api/[controller]/[action]")]
    public class ValuesController : Controller
    {
        readonly RawMortalityTableService service = new RawMortalityTableService();

        // GET api/values
        [Microsoft.AspNetCore.Mvc.HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }

        [HttpGet]
        public IActionResult GetRawMortalityTable(int age, int sexId)
        {           
            var rawMortalityTableInfo = service.GetbyAgeAndSex(age, sexId);
            return Ok(new { rawMortalityTableInfo });
        }
    }
}
