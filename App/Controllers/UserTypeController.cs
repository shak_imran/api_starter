﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using App.Core.Models.Security;
using App.Core.Services.Security;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;

namespace App.Controllers
{    
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [Route("api/[controller]/[action]")]
    public class UserTypeController : BaseController
    {
        readonly UserTypeService service = new UserTypeService();

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Get(int userId)
        {
            var userType = service.Get(userId);
            return Ok(userType);
        }

        [HttpGet]
        public IActionResult GetAll()   
        {
            var userTypes = service.GetAll(AppClaim.CompanyId.Value);
            return Ok(new { userTypes });
        }

        [HttpPost]
        public IActionResult Save(UserType userType)
        {
            userType.CD = userType.MD = CurrentDateTime;
            userType.CB = userType.MB = AppClaim.UserId ;
            var message = userType.UserTypeId == 0 ? service.Insert(userType) : service.Update(userType);
            return Ok(new { message });
        }
    }
}