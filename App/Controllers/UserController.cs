﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using App.Core.Models.Security;
using App.Core.Services.Security;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;

namespace App.Controllers
{    
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [Route("api/[controller]/[action]")]
    public class UserController : BaseController
    {
        readonly UserService service = new UserService();
        readonly UserCategoryService userCategoryService = new UserCategoryService();

        public IActionResult Index()
        {
            var userCategorySli = userCategoryService.UserCategorySli(AppClaim.CompanyId.Value);
            return Ok(userCategorySli);
        }

        public IActionResult Get(int userId)
        {
            var user = service.Get(userId, AppClaim.CompanyId.Value);
            return Ok(user);
        }

        [HttpGet]
        public IActionResult GetAll()   
        {
            var users = service.GetAll(AppClaim.CompanyId.Value).OrderByDescending(o=>o.MD);
            return Ok(new { users });
        }

        [HttpPost]
        public IActionResult Save(User user)
        {
            user.CompanyId = AppClaim.CompanyId;
            user.CD = user.MD = CurrentDateTime;
            user.CB = user.MB = AppClaim.UserId ;
            var message = user.UserId == 0 ? service.Insert(user) : service.Update(user);
            return Ok(new { message });
        }
    }
}