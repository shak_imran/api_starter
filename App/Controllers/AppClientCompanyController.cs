﻿using App.Core.Models.Security;
using App.Core.Services.Security;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace App.Controllers
{
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [Route("api/[controller]/[action]")]
    public class AppClientCompanyController : BaseController
    {
        readonly AppClientCompanyService service = new AppClientCompanyService();

        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public IActionResult Get(int companyId)
        {
            var company = service.Get(companyId);
            return Ok(company);
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            var appClientCompanies = service.GetAll();
            var appClientSli = new AppClientService().AppClientSli();
            var citySli = new CityService().CitySli();
            var stateSli = new StateService().StateSli();
            var countrySli = new CountryService().CountrySli();
            var currencySli = new CurrencyService().CurrencySli();

            return Ok(new { appClientCompanies , appClientSli, citySli, stateSli, countrySli, currencySli});
        }

        [HttpPost]
        public IActionResult Save(AppClientCompany company)
        {
            //appClient.ExpireDate = appClient.ExpireDateString.StringToDateTime();
            company.CB = company.MB = AppClaim.UserId;
            var message = company.CompanyId == 0 ? service.Insert(company) : service.Update(company);
            return Ok(new { message });
        }
    }
}