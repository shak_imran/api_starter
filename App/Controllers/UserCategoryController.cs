﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using App.Core.Models.Security;
using App.Core.Services.Security;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace App.Controllers
{
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [Route("api/[controller]/[action]")]
    public class UserCategoryController : BaseController
    {
        readonly UserCategoryService service = new UserCategoryService();

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Get(int userCategoryId)
        {
            var userType = service.Get(userCategoryId);
            return Ok(userType);
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            var userCategories = service.GetAll(AppClaim.CompanyId.Value);
            var userTypeSli = new UserTypeService().UserTypeSli();
            return Ok(new { userCategories , userTypeSli });
            
        }

        [HttpPost]
        public IActionResult Save(UserCategory userCategory)
        {
            userCategory.CB = userCategory.MB = AppClaim.UserId;
            var message = userCategory.UserCategoryId == 0 ? service.Insert(userCategory) : service.Update(userCategory);
            return Ok(new { message });
        }
    }
}