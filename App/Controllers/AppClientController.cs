﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using App.Core.Models.Security;
using App.Core.Services.Security;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace App.Controllers
{
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [Route("api/[controller]/[action]")]
    public class AppClientController : BaseController
    {
        readonly AppClientService service = new AppClientService();

        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public IActionResult Get(int appClientId)
        {
            var appClient = service.Get(appClientId);
            return Ok(appClient);
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            var appClients = service.GetAll();
            return Ok(new { appClients });
        }

        [HttpPost]
        public IActionResult Save(AppClient appClient)
        {
            //appClient.ExpireDate = appClient.ExpireDateString.StringToDateTime();
            appClient.CB = appClient.MB = AppClaim.UserId;
            var message = appClient.AppClientId == 0 ? service.Insert(appClient) : service.Update(appClient);
            return Ok(message);
        }
    }
}