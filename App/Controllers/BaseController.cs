﻿using System.Linq;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using App.Core.Models.Security;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using System;
using App.Core.Services.Security;

namespace App.Controllers
{
   
    public class BaseController : Controller
    {
        public AppClaim AppClaim;
     
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            var claims = HttpContext.User.Claims;

            string lCV(string key)
            {
                var val= claims.Where(o => o.Type == key).FirstOrDefault().Value;
                return val;
            }


            if (claims != null && claims.Count() > 0)
            {
                AppClaim = new AppClaim();
                AppClaim.UserId = lCV(nameof(AppClaim.UserId)).ToInt().Value;
                AppClaim.CompanyId = lCV(nameof(AppClaim.CompanyId)).ToInt();
                AppClaim.CustomerId = lCV(nameof(AppClaim.CustomerId)).ToInt();
                AppClaim.UserCategoryId = lCV(nameof(AppClaim.UserCategoryId)).ToInt().Value;
                AppClaim.OffsetInMinutes = lCV(nameof(AppClaim.OffsetInMinutes)).ToInt().Value;
                AppClaim.UserName = lCV(nameof(AppClaim.UserName));
                AppClaim.UserCategoryName = lCV(nameof(AppClaim.UserCategoryName));
                AppClaim.NeedToChangePassword = Convert.ToBoolean(lCV(nameof(AppClaim.NeedToChangePassword)));
                AppClaim.TimeFormat = lCV(nameof(AppClaim.TimeFormat));
                AppClaim.DateFormat = lCV(nameof(AppClaim.DateFormat));

            }
            else
            {
                AppClaim = null;
            }



            base.OnActionExecuting(context);
        }


        public IActionResult CitySli(int? stateId=null)
        {
            var citySli = new CityService().CitySli(stateId);
            return Ok(citySli);
        }

        public string GetBaseUrl()
        {
            return $"{HttpContext.Request.Scheme}://{HttpContext.Request.Host}";
        }

        public DateTime CurrentDate
        {
            get
            {
                if (AppClaim != null && AppClaim.OffsetInMinutes > 0)
                {
                    return DateTime.UtcNow.AddMinutes(AppClaim.OffsetInMinutes);
                }
                return DateTime.UtcNow;
            }
        }

        public DateTime CurrentDateTime
        {
            get
            {
                
                if (AppClaim != null && AppClaim.OffsetInMinutes > 0)
                {
                    var d = DateTime.UtcNow.AddMinutes(AppClaim.OffsetInMinutes);
                    return new DateTime(d.Year, d.Month, d.Day);
                }
                else
                {
                    var d = DateTime.UtcNow;
                    return new DateTime(d.Year, d.Month, d.Day);
                }
            }
        }
    }
}