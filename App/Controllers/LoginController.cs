﻿
using Microsoft.IdentityModel.Tokens;
using System.Text;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System;
using System.Security.Claims;
using App.Core.Services.Security;
using App.Core.Utilities;
using App.Core.Models.Security;
using App.Core.Services.Configuration;
using App.Core.Models.Configuration;

namespace App.Controllers
{

    public class LoginController : Controller
    {
        readonly IConfiguration _config;
        UserService userService;
        const int tokenExpireInHours = 4; // Minutes

        public LoginController(IConfiguration config)
        {
            _config = config;
        }

        [AllowAnonymous]
        [HttpPost]
        [Route("api/token")]
        public IActionResult GenerateToken(LoginViewModel model)
        {
            try
            {
                userService = new UserService();
                var user = userService.Get(model.UserName);
                if (IsValidUser(user, model.Password))
                {
                    var modules = user.UserCategoryId != (int)Enums.UserCategories.Customer ? userService.Screens(user.UserId, user.UserCategoryId) : new List<Module>();

                    Customer customer = null;
                    if (user.UserCategoryId == (int)Enums.UserCategories.Customer)
                    {
                        customer = new CustomerService().GetByUserId(user.UserId);
                    }

                    var claims = new List<Claim>();

                    claims.Add(new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()));
                    claims.Add(new Claim(nameof(AppClaim.CompanyId), user.CompanyId == null ? "" : user.CompanyId.ToString()));
                    claims.Add(new Claim(nameof(AppClaim.CustomerId), customer == null ? "" : customer.CustomerId.ToString()));
                    claims.Add(new Claim(nameof(AppClaim.UserId), user.UserId.ToString()));
                    claims.Add(new Claim(nameof(AppClaim.UserName), user.UserName.ToString()));
                    claims.Add(new Claim(nameof(AppClaim.UserCategoryId), user.UserCategoryId.ToString()));
                    claims.Add(new Claim(nameof(AppClaim.UserCategoryName), user.UserCategoryName.ToString()));
                    claims.Add(new Claim(nameof(AppClaim.NeedToChangePassword), user.NeedToChangePassword.ToString()));
                    claims.Add(new Claim(nameof(AppClaim.OffsetInMinutes), user.OffsetInMinutes == null ? Constants.OffsetInMinutes.ToString() : user.OffsetInMinutes.ToString()));
                    claims.Add(new Claim(nameof(AppClaim.DateFormat), user.DateFormat == null ? Constants.DefaultDateFormat : user.DateFormat));
                    claims.Add(new Claim(nameof(AppClaim.TimeFormat), user.TimeFormat == null ? Constants.DefaultTimeFormat : user.TimeFormat));


                    var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["Tokens:Key"]));
                    var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

                    var token = new JwtSecurityToken(_config["Tokens:Issuer"],
                      _config["Tokens:Issuer"],
                      claims,
                      expires: DateTime.UtcNow.AddHours(tokenExpireInHours),
                      signingCredentials: creds);


                    return Ok(new
                    {
                        Token = new JwtSecurityTokenHandler().WriteToken(token),
                        ExpireInHours = tokenExpireInHours,
                        DateFormat = user.DateFormat == null ? Constants.DefaultDateFormat : user.DateFormat,
                        TimeFormat = user.TimeFormat == null ? Constants.DefaultTimeFormat : user.TimeFormat,
                        ImageBaseUrl =$"{GetBaseUrl()}/uploads",
                        User = new
                        {
                            UserName = user.UserName,
                            UserCategoryName = user.UserCategoryName,
                            UserCategoryId = user.UserCategoryId,
                            NeedToChangePassword = user.NeedToChangePassword
                        },
                        //Customer = customer == null ? null : new
                        //{
                        //    CustomerId = customer.CustomerId,
                        //    ImageName=customer.ImageName,
                        //    FirstName = customer.FirstName,
                        //    LastName = customer.LastName,
                        //    CD = string.Format("{0:MMM, yyyy}", customer.CD)
                        //},
                        Modules = modules
                    });

                }
                else
                {
                    return BadRequest("Invalid username or password or your account is inactive");
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

            //return BadRequest("Could not create token");
        }

        private bool IsValidUser(User user, string enteredPassword)
        {
            if (user != null)
            {
                if (user.IsActive)
                {
                    if (user.Password.Equals(Encription.Encrypt(enteredPassword)))
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        private string GetBaseUrl()
        {
            return $"{HttpContext.Request.Scheme}://{HttpContext.Request.Host}";
        }
    }

    public class LoginViewModel
    {
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}