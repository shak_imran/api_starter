﻿using System;
using System.Collections.Generic;
using System.Text;

namespace App.Core.Utilities
{
    public struct AppDateTime
    {
        public static DateTime Now
        {
            get
            {
                //var appClaim = ClaimHelper.GetCurrentClaim();
                //if (appClaim != null && appClaim.OffsetInMinutes > 0)
                //{
                //    return DateTime.UtcNow.AddMinutes(appClaim.OffsetInMinutes);
                //}
                return DateTime.UtcNow;
            }
        }

        public static DateTime Date
        {

            get
            {
                //var appClaim = ClaimHelper.GetCurrentClaim();
                //if (appClaim != null && appClaim.OffsetInMinutes > 0)
                //{
                //    var d = DateTime.UtcNow.AddMinutes(appClaim.OffsetInMinutes);
                //    return new DateTime(d.Year, d.Month, d.Day);
                //}
                //else
                //{
                    var d = DateTime.UtcNow;
                    return new DateTime(d.Year, d.Month, d.Day);
                //}
                
            }           
        }
    }
}
