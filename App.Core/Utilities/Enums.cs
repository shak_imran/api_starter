﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace App
{
    public class Enums
    {
        public enum MessageTypes
        {
            None = 0,
            Success = 1,
            Error = 2,
            Warning = 3,
        }

        public enum UserCategories
        {
            [Description("")]
            SystemAdmin = 1,
            CompanyAdmin = 2,
            Customer = 3,
            ReferenceUser = 4
        }

        public enum Relationships
        {
            [Description("Parent")]
            Parent = 1,
            [Description("Child")]
            Child = 2,
            [Description("Spouse")]
            Spous2 = 3,
            [Description("Sibling")]
            Sibling = 4,
            [Description("Grandparents")]
            Grandparents = 5,
            [Description("Grandchild")]
            Grandchild = 6,
            [Description("Parent's sibling")]
            ParentSibling = 7,
            [Description("Sibling's child")]
            SiblingChild = 8,
            [Description("Aunt's/uncle's child")]
            AuntUncleChild = 9
        }

        public enum PaymentFrequency
        {
            [Description("Monthly")]
            Monthly = 12,
            [Description("Yearly")]
            Yearly = 1
        }

        public enum PensionAmountSourceTypes
        {
            [Description("Self")]
            SelftWithdrawal = 1,
            [Description("Withdrawal Request")]
            RequestForWithdrawal = 2
        }

        public enum CustomerQuoteStatus
        {
            [Description("Pending")]
            Pending = 1,
            [Description("Accepted")]
            Accepted = 2,
            [Description("Rejected")]
            Rejected = 3
        }
        public enum PensionPercent
        {
            [Description("0%")]
            Zero_Percent = 0,
            [Description("10%")]
            Ten_Percent = 10,
            [Description("25%")]
            TwentyFive_Percent = 25,
            [Description("50%")]
            Fifty_Percent = 50,
            [Description("75%")]
            SeventyFifty_Percent = 75,
            [Description("100%")]
            Hundred_Percent = 100,
            [Description("125%")]
            Hundred_Twenty_Five_Percent = 125,
            [Description("150%")]
            Hundred_Fifty_Percent = 150
        }
        public enum MortalityTable
        {
            [Description("a(55)")]
            a_55 = 1,
            [Description("a(90)")]
            a_90 = 2,
            [Description("pa(90)")]
            pa_90 = 3,
            [Description("Others")]
            Others = 4
        }

        public enum PricingParticulars
        {
            [Description("Annually")]
            Annually = 1,
            [Description("Semi Annually")]
            Semi_Annually = 2,
            [Description("Quarterly")]
            Quarterly = 4,
            [Description("Monthly")]
            Monthly = 12,
            [Description("Weekly")]
            Weekly = 52
        }

        public enum PensionTypes
        {
            [Description("Life")]
            Life = 1,
            [Description("Term")]
            Term = 2
        }

        public enum Gender
        {
            [Description("Male")]
            Male = 1,
            [Description("Female")]
            Female = 2
        }

        public enum PensionEscalationFactor
        {
            [Description("0%")]
            Zero = 0,
            [Description("1%")]
            One = 1,
            [Description("2%")]
            Two = 2,
            [Description("3%")]
            Three = 3,
            [Description("4%")]
            Four = 4,
            [Description("5%")]
            Five = 5
        }

        public enum MaritalStatus
        {
            [Description("Single")]
            Single = 1,
            [Description("Married")]
            Married = 2,
            [Description("Divorced")]
            Divorced = 3
        }

    }
}
