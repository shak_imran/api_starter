﻿using System;
using System.Collections.Generic;
using System.Text;

namespace App
{
    public class Constants
    {
        public static string DefaultConnectionString { get; set; }
        public const int OffsetInMinutes = 360;
        public const string DefaultDateFormat = "dd-MM-yyyy";
        public const string DefaultTimeFormat = "hh:mm TT";
        public const int DefaultCounteryId = 1;
        public const string GlobalDeleteMessage = "Data deleted successfully";
        public const string GlobalSuccessMessage = "Data saved successfully";
        public const string GlobalErrorMessage = "Error occurred, data not saved.";
        public const string InsuranceCompanyLogo = "Uploads/InsuranceCompanyLogo";
        public const string UserImagePath = "Uploads/UserImage";
        public const string CustomerImagePath = "Uploads/CustomerImage";
        public const string InsuranceCompanyStatImagePath = "Uploads/InsuranceCompanyStatImage";
    }
}
