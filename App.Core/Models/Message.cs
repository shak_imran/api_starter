﻿using App.Core.Utilities;
using System;
using System.Collections.Generic;
using System.Text;

namespace App.Core.Models
{
    public class Message
    {
        [NotMaped]
        public Enums.MessageTypes MessageType { get; set; }

        [NotMaped]
        public string MessageString { get; set; }

        [NotMaped]
        public string MessageDetails { get; set; }

        public Message()
        {
            MessageType = Enums.MessageTypes.None;
        }
    }
}
