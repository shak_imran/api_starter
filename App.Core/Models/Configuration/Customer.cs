﻿using App.Core.Utilities;
using System;
using System.Collections.Generic;
using System.Text;

namespace App.Core.Models.Configuration
{
    public class Customer : Base
    {
        [PKey]
        public int CustomerId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string MobileNo { get; set; }
        public DateTime? DOB { get; set; }
        public string Gender { get; set; }
        [NotMaped]
        public string DOBString { get; set; }
        public int? Pre_StateId { get; set; }
        public int? Pre_CityId { get; set; }
        public string Pre_Address { get; set; }
        public string Pre_ZipCode { get; set; }
        public int? Per_StateId { get; set; }
        public int? Per_CityId { get; set; }
        public string Per_Address { get; set; }
        public string Per_ZipCode { get; set; }
        public string PIN { get; set; }
        public string NID { get; set; }
        public string EmergencyMobileNo { get; set; }
        public int UserId { get; set; }
        public string ImageName { get; set; }

        [NotMaped]
        public string Password { get; set; }
        [NotMaped]
        public int CustomerAge { get; set; }
    }
}
