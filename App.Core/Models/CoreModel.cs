﻿using App.Core.Utilities;
using System;
using System.Collections.Generic;
using System.Text;

namespace App.Core.Models
{
    public class CoreModel:Message
    {
        [NotMaped]
        public int Rows { get; set; }
    }
}
