﻿using System;
using System.Collections.Generic;
using System.Text;

namespace App.Core.Models
{
    public class Base : CoreModel
    {
        public DateTime CD { get; set; }
        public DateTime MD { get; set; }
        public int CB { get; set; }
        public int MB { get; set; }
    }
}
