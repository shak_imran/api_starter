﻿using App.Core.Utilities;
using System;
using System.Collections.Generic;
using System.Text;

namespace App.Core.Models.Security
{
    [Table("usercategory", "appdb")]
    public class UserCategory : Base
    {
        [PKey]
        public int UserCategoryId { get; set; }
        public int? CompanyId { get; set; }
        public string UserCategoryName { get; set; }
        public string Description { get; set; }
        public int UserTypeId { get; set; }
        public bool IsSystemReserved { get; set; }
    }
}
