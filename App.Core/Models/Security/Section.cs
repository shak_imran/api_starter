﻿using App.Core.Utilities;
using System;
using System.Collections.Generic;
using System.Text;

namespace App.Core.Models.Security
{
    public class Section: Base
    {
        [PKey]
        public int SectionId { get; set; }
        public int SubModuleId { get; set; }
        public string SectionName { get; set; }
        public string IconName { get; set; }
        public short Ordinal { get; set; }

        public IEnumerable<Screen> Screens { get; set; }
    }
}
