﻿using App.Core.Utilities;
using System;
using System.Collections.Generic;
using System.Text;

namespace App.Core.Models.Security
{
    [Table("state", "appdb")]
    public class State : Base
    {
        [PKey]
        public int StateId { get; set; }
        public int CountryId { get; set; }
        public string Name { get; set; }
    }
}
