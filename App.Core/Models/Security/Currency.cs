﻿using App.Core.Utilities;
using System;
using System.Collections.Generic;
using System.Text;

namespace App.Core.Models.Security
{
    [Table("currency")]
    public class Currency 
    {
        [PKey]
        public int CurrencyId { get; set; }
        public string CurrencyName { get; set; }
        public string Code { get; set; }
        public string Symbol { get; set; }
    }
}
