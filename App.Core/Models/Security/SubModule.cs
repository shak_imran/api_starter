﻿using App.Core.Utilities;
using System;
using System.Collections.Generic;
using System.Text;

namespace App.Core.Models.Security
{
    public class SubModule :  Base
    {
        [PKey]
        public int SubModuleId { get; set; }
        public int ModuleId { get; set; }
        public string SubModuleName { get; set; }
        public string AreaName { get; set; }
        public string SubModuleIconName { get; set; }
        public short Ordinal { get; set; }

        public IEnumerable<Section> Sections { get; set; }
    }
}
