﻿using App.Core.Utilities;
using System;
using System.Collections.Generic;
using System.Text;

namespace App.Core.Models.Security
{
    [Table("appclientcompany", "rssdb")]
    public class AppClientCompany: Base
    {
        [PKey]
        public int CompanyId { get; set; }
        public int AppClientId { get; set; }
        public string CompanyName { get; set; }
        public string Email { get; set; }
        public byte Logo { get; set; }
        public string DateFormat { get; set; }
        public string TimeFormat { get; set; }
        public int CurrencyId { get; set; }
        public int TimeZoneId { get; set; }
        public string Address { get; set; }
        public int? CountryId { get; set; }
        public int? StateId { get; set; }
        public int? CityId { get; set; }
        public string ZipCode { get; set; }
    }
}
