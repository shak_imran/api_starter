﻿using App.Core.Utilities;
using System;
using System.Collections.Generic;
using System.Text;

namespace App.Core.Models.Security
{
    [Table("country", "rssdb")]
    public class Country
    {
        [PKey]
        public int CountryId { get; set; }
        public string Name { get; set; }
        public string Nationality { get; set; }
        public string ISO3Code { get; set; }
        public string ISO2Code { get; set; }
        public int TeleIndex { get; set; }
        public string Flag { get; set; }
    }
}
