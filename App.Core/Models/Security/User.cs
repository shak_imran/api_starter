﻿using App.Core.Utilities;
using System;
using System.Collections.Generic;
using System.Text;


namespace App.Core.Models.Security
{
    [Table("user", "appdb")]
    public class User : Base
    {
        [PKey]
        public int UserId { get; set; }
        public int? CompanyId { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public int UserCategoryId { get; set; }
        public bool NeedToChangePassword { get; set; }
        public string Email { get; set; }
        public bool IsActive { get; set; }

        [NotMaped]
        public string UserCategoryName { get; set; }
        [NotMaped]
        public string DateFormat { get; set; }
        [NotMaped]
        public string TimeFormat { get; set; }
        [NotMaped]
        public int? OffsetInMinutes { get; set; }
    }
}
