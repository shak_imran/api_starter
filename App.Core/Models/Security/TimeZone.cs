﻿using App.Core.Utilities;
using System;
using System.Collections.Generic;
using System.Text;

namespace App.Core.Models.Security
{
    public class TimeZone  : Base
    {
        [PKey]
        public int TimeZoneId { get; set; }
        public string Name { get; set; }
        public string Offset { get; set; }
        public int OffsetInMinutes { get; set; }
    }
}
