﻿using App.Core.Utilities;
using System;
using System.Collections.Generic;
using System.Text;

namespace App.Core.Models.Security
{
    [Table("usertype", "rssdb")]
    public class UserType : Base
    {
        [PKey]
        public int UserTypeId { get; set; }
        public int? CompanyId { get; set; }
        public string UserTypeName { get; set; }
        public bool IsSystemReserved { get; set; }
    }
}
