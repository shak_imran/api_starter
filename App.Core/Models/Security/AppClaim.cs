﻿using System;
using System.Collections.Generic;
using System.Text;

namespace App.Core.Models.Security
{
    public class AppClaim
    {
        public int UserId { get; set; }
        public int? CompanyId { get; set; }
        public int? CustomerId { get; set; }
        public string UserName { get; set; }
        public int UserCategoryId { get; set; }
        public string UserCategoryName { get; set; }
        public bool NeedToChangePassword { get; set; }
        public int OffsetInMinutes { get; set; }
        public string DateFormat { get; set; }
        public string TimeFormat { get; set; }
    }
}
