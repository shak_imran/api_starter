﻿using App.Core.Utilities;
using System;
using System.Collections.Generic;
using System.Text;

namespace App.Core.Models.Security
{
    public class Module : Base
    {
        [PKey]
        public int ModuleId { get; set; }
        public string ModuleName { get; set; }
        public short Ordinal { get; set; }

        public IEnumerable<SubModule> SubModules { get; set; }
    }
}
