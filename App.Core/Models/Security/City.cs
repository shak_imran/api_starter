﻿using App.Core.Utilities;
using System;
using System.Collections.Generic;
using System.Text;

namespace App.Core.Models.Security
{
    [Table("city", "appdb")]
    public class City : Base
    {
        [PKey]
        public int CityId { get; set; }
        public int StateId { get; set; }
        public int CompanyId { get; set; }
        public string Name { get; set; }
    }
}
