﻿using App.Core.Utilities;
using System;
using System.Collections.Generic;
using System.Text;

namespace App.Core.Models.Security
{
    public class UserWiseScreen : Base
    {
        [PKey]
        public int Id { get; set; }
        public int UserId { get; set; }
        public int ScreenId { get; set; }
        public bool Read { get; set; }
        public bool Create { get; set; }
        public bool Update { get; set; }
        public bool Delete { get; set; }
    }
}
