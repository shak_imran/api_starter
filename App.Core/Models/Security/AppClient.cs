﻿using App.Core.Utilities;
using System;

namespace App.Core.Models.Security
{
    [Table("appclient")]
    public class AppClient : Base
    {
        [PKey]
        public int AppClientId { get; set; }
        public string AppClientName { get; set; }
        public DateTime ExpireDate { get; set; }
        public int NumberOfCompanies { get; set; }
        public int NumberOfBranches { get; set; }
        public int? NumberOfUsers { get; set; }
        public int? NumberOfEmployees { get; set; }

        [NotMaped]
        public string ExpireDateString { get; set; }
    }
}
