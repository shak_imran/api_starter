﻿using App.Core.Utilities;
using System;
using System.Collections.Generic;
using System.Text;

namespace App.Core.Models.Security
{
    public class Screen : Base
    {
        [PKey]
        public int ScreenId { get; set; }
        public int SectionId { get; set; }
        public string ScreenName { get; set; }
        public string RoutePath { get; set; }
        public string IconName { get; set; }
        public short Ordinal { get; set; }
        
       
        [NotMaped]
        public bool Read { get; set; }

        [NotMaped]
        public bool Create { get; set; }

        [NotMaped]
        public bool Update { get; set; }

        [NotMaped]
        public bool Delete { get; set; }
    }
}
