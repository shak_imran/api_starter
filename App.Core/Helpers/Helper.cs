﻿using App.Core.Utilities;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Reflection;


namespace App
{
    public static class Helper
    {
        public static T Success<T>(this T entity, string message = null)
        {
            var type = entity.GetType();

            PropertyInfo messageTypePI = type.GetProperty("MessageType");
            if (messageTypePI != null)
            {
                messageTypePI.SetValue(entity, Convert.ChangeType(Enums.MessageTypes.Success, messageTypePI.PropertyType), null);
            }
            PropertyInfo messageStringPI = type.GetProperty("MessageString");
            if (messageStringPI != null)
            {
                messageStringPI.SetValue(entity, Convert.ChangeType(message != null ? message : Constants.GlobalSuccessMessage, messageStringPI.PropertyType), null);
            }

            return entity;
        }

        public static T Error<T>(this T entity, string message = null, string messageDetails = null)
        {
            var type = entity.GetType();

            PropertyInfo messageTypePI = type.GetProperty("MessageType");
            if (messageTypePI != null)
            {
                messageTypePI.SetValue(entity, Convert.ChangeType(Enums.MessageTypes.Error, messageTypePI.PropertyType), null);
            }
            PropertyInfo messageStringPI = type.GetProperty("MessageString");
            if (messageStringPI != null)
            {
                messageStringPI.SetValue(entity, Convert.ChangeType(message != null ? message : Constants.GlobalErrorMessage, messageStringPI.PropertyType), null);
            }

            if (messageDetails.IsNotNullOrEmpty())
            {
                PropertyInfo messageDetailsPI = type.GetProperty("MessageDetails");
                if (messageDetailsPI != null)
                {
                    messageDetailsPI.SetValue(entity, Convert.ChangeType(messageDetails, messageDetailsPI.PropertyType), null);
                }
            }

            return entity;
        }

        public static bool IsNotNullOrEmpty(this string str)
        {
            return !string.IsNullOrWhiteSpace(str);
        }

       

        public static IEnumerable<SelectListItem> ToSelectList<T>(object setdValue = null, string defalutText = "", string defalutValue = "")
        {
            var items = Enum.GetValues(typeof(T)).Cast<T>().Select(v => new SelectListItem
            {
                Text = v.ToDescription(),
                Value = Convert.ToInt32(v).ToString()
            }).ToList();

            if (defalutText.IsNotNullOrEmpty())
            {
                items.Insert(0, new SelectListItem { Text = defalutText, Value = defalutValue });
            }

            if (setdValue != null)
            {
                items.SetSelected(setdValue);
            }

            return items;
        }

        public static string ToDescription<T>(this T status)
        {
            Type enumType = typeof(T);

            MemberInfo memberInfo =
                enumType.GetMember(status.ToString()).First();
            var descriptionAttribute =
                memberInfo.GetCustomAttribute<DescriptionAttribute>();

            var description = descriptionAttribute.Description;

            return description;
        }


        public static List<SelectListItem> SetSelected(this List<SelectListItem> items, object value)
        {
            if (items != null && value != null)
            {
                var v = value.ToString();
                if (items.Any())
                {
                    foreach (var s in items.Where(o => o.Selected == true))
                    {
                        s.Selected = false;
                    }

                    var item = items.Where(o => o.Value == v).FirstOrDefault();
                    if (item != null)
                    {
                        item.Selected = true;
                    }
                }
            }
            return items;
        }

        public static string ByteToImage(this byte[] image)
        {
            if (image == null)
            {
                return "/Content/images/no-image.png";
            }
            return String.Format("data:image/gif;base64,{0}", Convert.ToBase64String(image));
        }

        public static IEnumerable<SelectListItem> DateFormatSli(string selectedValue = null)
        {
            var lst = new List<SelectListItem> {
                    new SelectListItem{Value="yyyy-MM-dd" , Text="yyyy-MM-dd"},
                    new SelectListItem{Value="dd/MM/yyyy" , Text="dd/MM/yyyy"},
                    new SelectListItem{Value="d/MM/yyyy" , Text="d/MM/yyyy"},
                    new SelectListItem{Value="dd.MM.yyyy" , Text="dd.MM.yyyy"},
                    new SelectListItem{Value="yyyy-M-d" , Text="yyyy-M-d"},
                    new SelectListItem{Value="d.M.yyyy" , Text="d.M.yyyy"},
                    new SelectListItem{Value="dd-MM-yyyy" , Text="dd-MM-yyyy"},
                    new SelectListItem{Value="MM/dd/yyyy" , Text="MM/dd/yyyy"},
                    new SelectListItem{Value="d.MM.yyyy" , Text="d.MM.yyyy"},
                    new SelectListItem{Value="d/M/yyyy" , Text="d/M/yyyy"},
                    new SelectListItem{Value="MM-dd-yyyy" , Text="MM-dd-yyyy"},
                    new SelectListItem{Value="dd.MM.yyyy" , Text="dd.MM.yyyy"},
                    new SelectListItem{Value="yyyy.MM.dd" , Text="yyyy.MM.dd"},
                    new SelectListItem{Value="yyyy/MM/dd" , Text="yyyy/MM/dd"},
                    new SelectListItem{Value="yyyy.M.d" , Text="yyyy.M.d"},
                    new SelectListItem{Value="yyyy.d.M" , Text="yyyy.d.M"},
                    new SelectListItem{Value="d.M.yyyy" , Text="d.M.yyyy"},
                    new SelectListItem{Value="d-M-yyyy" , Text="d-M-yyyy"},
                    new SelectListItem{Value="M/d/yyyy" , Text="M/d/yyyy"},
                    new SelectListItem{Value="yyyy/M/d" , Text="yyyy/M/d"}
            };

            if (selectedValue != null)
            {
                lst.SetSelected(selectedValue);
            }

            return lst;
        }

        public static DateTime StringToDate(this string stringDate, string dateFormat)
        {
            return DateTime.ParseExact(stringDate, dateFormat, CultureInfo.InvariantCulture);
        }

        public static DateTime StringToDateTime(this string stringDate, string dateFormat)
        {
            dateFormat = $"{dateFormat} hh:mm tt";
            return DateTime.ParseExact(stringDate, dateFormat, CultureInfo.InvariantCulture);
        }

        public static string DateToString(this DateTime date, string dateFormat)
        {
            if (date <= DateTime.MinValue)
            {
                return string.Empty;
            }

            return date.ToString(dateFormat);
        }

        public static string DateToString(this DateTime? date, string dateFormat)
        {
            if (date == null)
            {
                return string.Empty;
            }
            return date.Value.ToString(dateFormat);
        }

        public static string DateTimeToString(this DateTime date, string dateFormat)
        {
            dateFormat = $"{dateFormat} hh:mm tt";
            return date.ToString(dateFormat);
        }

        public static string DateTimeToString(this DateTime? date, string dateFormat)
        {
            if (date == null)
                return string.Empty;

            dateFormat = $"{dateFormat} hh:mm tt";
            return date.Value.ToString(dateFormat);
        }

        public static IEnumerable<SelectListItem> MonthSelectList(string value = null, string defaultText = "Select Month", string defaultValue = "")
        {

            var items = new List<SelectListItem>();
            for (int i = 1; i <= 12; i++)
            {
                items.Add(new SelectListItem { Text = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(i), Value = i.ToString() });
            }

            items.Insert(0, new SelectListItem { Text = "Select Month", Value = defaultValue });


            if (value.IsNotNullOrEmpty())
            {
                items.Where(o => o.Value == value).FirstOrDefault().Selected = true;
            }

            return items;
        }
       
        //public static string NewFileName(this HttpPostedFileBase doc, string oldFileName)
        //{
        //    if (doc != null && doc.ContentLength > 0)
        //    {
        //        var extenstion = Path.GetExtension(doc.FileName);
        //        return Guid.NewGuid() + extenstion;
        //    }

        //    return oldFileName;
        //}

        //public static string NewFileName(this HttpPostedFile doc)
        //{
        //    if (doc != null && doc.ContentLength > 0)
        //    {
        //        var extenstion = Path.GetExtension(doc.FileName);
        //        return Guid.NewGuid() + extenstion;
        //    }

        //    return null;
        //}

        //public static void SaveDoc(this HttpPostedFileBase doc, string path, string oldFileName, string newFileName)
        //{
        //    if (doc != null)
        //    {
        //        if (oldFileName.IsNotNullOrEmpty())
        //        {
        //            string fullPath = Path.Combine(path, oldFileName);
        //            if (File.Exists(fullPath))
        //            {
        //                File.Delete(fullPath);
        //            }
        //        }

        //        var uploadPath = Path.Combine(path, newFileName);
        //        doc.SaveAs(uploadPath);
        //    }
        //}

        public static int? ToInt(this string value)
        {
            if (string.IsNullOrEmpty(value))
                return null;


            return Convert.ToInt32(value);
        }

        public static int OffsetToMinutes(this string offset)
        {
            var totalMinutes = 0;

            if (offset.IsNotNullOrEmpty())
            {
                var sign = offset.Contains("-") ? -1 : 1;
                offset = offset.Trim().Replace("-", "").Replace("+", "").Replace(" ", "");
                var s = offset.Split(':');
                var hours = Convert.ToInt32(s[0]);
                var minutes = Convert.ToInt32(s[1]);
                totalMinutes = minutes + (hours * 60) * sign;
            }

            return totalMinutes;
        }
    }
}
