﻿using App.Core.Models;
using App.Core.Utilities;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace App.Core.Services
{
    public abstract class BaseService : IDisposable
    {
        public MySqlConnection Connection { get; set; }
        public Message Message { get; set; }
        bool disposed = false;

        public BaseService()
        {
            Connection = new MySqlConnection(Constants.DefaultConnectionString);
            OpenConnection();
            Message = new Message();
        }

        public void OpenConnection()
        {
            if (Connection.State != ConnectionState.Open)
            {
                Connection.Open();
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }


        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;

            if (disposing)
            {
                // Free any other managed objects here.

            }

            // Free any unmanaged objects here.
            //
            if (Connection != null)
            {
                Connection.Dispose();
            }
            disposed = true;
        }

        ~BaseService()
        {
            Dispose(false);
        }
    }

    public interface IBaseService<TEntity> where TEntity : class
    {
        IEnumerable<TEntity> GetAll();
        TEntity Get(int id);
        Message Insert(TEntity entity);
        Message Update(TEntity entity);
        Message Delete(int id);
    }
}
