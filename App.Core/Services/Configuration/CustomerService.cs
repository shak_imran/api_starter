﻿using App.Core.Models.Configuration;
using App.Core.Repositories.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using App.Core.Models;
using App.Core.Repositories.Security;
using App.Core.Models.Security;
using App.Core.Utilities;

namespace App.Core.Services.Configuration
{
    public class CustomerService : BaseService, IBaseService<Customer>
    {
        readonly CustomerRepository customerRepository;

        public CustomerService()
        {
            customerRepository = new CustomerRepository(Connection);
        }

        public Message Delete(int id)
        {
            throw new NotImplementedException();
        }

        public Customer Get(int id)
        {
            try
            {
                return customerRepository.Get(id);
            }
            catch (Exception ex)
            {
                throw new Exception("Exception occurred, please contact with system admin.");
            }
            finally
            {
                Connection.Close();
            }
        }

        public Customer GetByQuote(int qouteId)
        {
            try
            {
                return customerRepository.GetByQuote(qouteId);
            }
            catch (Exception ex)
            {
                throw new Exception("Exception occurred, please contact with system admin.");
            }
            finally
            {
                Connection.Close();
            }

        }

        public Customer GetByEmail(string email)
        {
            try
            {
                return customerRepository.GetByEmail(email);
            }
            catch (Exception ex)
            {
                throw new Exception("Exception occurred, please contact with system admin.");
            }
            finally
            {
                Connection.Close();
            }

        }

        public Message SaveImageName(int customerId, string imageName)
        {
            try
            {
                customerRepository.SaveImageName(customerId, imageName);
                Message.Success();
            }
            catch (Exception ex)
            {
                Message.Error(messageDetails: ex.Message);
            }
            finally
            {
                Connection.Close();
            }

            return Message;
        }

       

        public Customer GetByUserId(int userId)
        {
            try
            {
                return customerRepository.GetByUserId(userId);
            }
            catch (Exception ex)
            {
                throw new Exception("Exception occurred, please contact with system admin.");
            }
            finally
            {
                Connection.Close();
            }

        }

        public IEnumerable<Customer> GetAll()
        {
            try
            {
                return customerRepository.GetAll();
            }
            catch (Exception ex)
            {
                throw new Exception("Exception occurred, please contact with system admin.");
            }
            finally
            {
                Connection.Close();
            }

        }

        public Message Insert(Customer entity)
        {
            try
            {
                Connection.Open();
                using (var transaction = Connection.BeginTransaction())
                {
                    try
                    {
                        customerRepository.SetTransction(transaction);
                        var userRepository = new UserRepository(Connection);
                        var user = new User
                        {
                            UserName = entity.Email,
                            UserCategoryId = (int)Enums.UserCategories.Customer,
                            NeedToChangePassword = true,
                            Password = Encription.Encrypt(entity.Password),
                            Email = entity.Email,
                            IsActive = true,
                            CD = entity.CD,
                            MD = entity.MD
                        };
                        user.UserId = userRepository.Insert(user);
                        entity.UserId = user.UserId;
                        entity.CB = entity.MB = user.UserId;
                        entity.CustomerId = customerRepository.Insert(entity);
                        Message.Success();
                        transaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        Message.Error(messageDetails: ex.Message);
                        transaction.Rollback();
                    }

                }
            }
            catch (Exception ex)
            {
                Message.Error(messageDetails: ex.Message);
            }
            finally
            {
                Connection.Close();
            }

            return Message;
        }

        public Message Update(Customer entity)
        {
            try
            {
                customerRepository.Update(entity);
                Message.Success();
            }
            catch (Exception ex)
            {
                Message.Error(messageDetails: ex.Message);
            }
            finally
            {
                Connection.Close();
            }

            return Message;
        }

        public Message SavePersonalDetails(Customer entity)
        {
            try
            {
                customerRepository.SavePersonalDetails(entity);
                Message.Success();
            }
            catch (Exception ex)
            {
                Message.Error(messageDetails: ex.Message);
            }
            finally
            {
                Connection.Close();
            }

            return Message;
        }

        public Message SavePersonalAddress(Customer entity)
        {
            try
            {
                customerRepository.SavePersonalAddress(entity);
                Message.Success();
            }
            catch (Exception ex)
            {
                Message.Error(messageDetails: ex.Message);
            }
            finally
            {
                Connection.Close();
            }

            return Message;
        }

        public (int numberOfQuote, int numberOfCompany) NumOfQuoteAndFCompany(int customerId)
        {
            try
            {
                return customerRepository.NumOfQuoteAndFCompany(customerId);
            }
            catch (Exception ex)
            {
                throw new Exception("Exception occurred, please contact with system admin.");
            }
            finally
            {
                Connection.Close();
            }

        }
    }
}
