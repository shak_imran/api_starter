﻿using App.Core.Models;
using App.Core.Models.Security;
using App.Core.Repositories.Security;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace App.Core.Services.Security
{
    public class UserTypeService : BaseService, IBaseService<UserType>
    {
        readonly UserTypeRepository repository;

        public UserTypeService()
        {
            repository = new UserTypeRepository(Connection);
        }

        public Message Delete(int id)
        {
            throw new NotImplementedException();
        }

        public UserType Get(int id)
        {
            return repository.Get(id);
        }

        public IEnumerable<UserType> GetAll()
        {
            return repository.GetAll();
        }

        public IEnumerable<UserType> GetAll(int companyId)
        {
            return repository.GetAll();
        }

        public Message Insert(UserType entity)
        {
            try
            {
                entity.CD = entity.MD = DateTime.Now;
                entity.UserTypeId = repository.Insert(entity);
                Message.Success();
            }
            catch(Exception ex)
            {
                Message.Error(messageDetails: ex.Message);
            }
            finally
            {
                Connection.Close();
            }

            return Message;
        }

        public Message Update(UserType entity)
        {
            try
            {
                entity.MD = DateTime.Now;
                repository.Update(entity);
                Message.Success();
            }
            catch(Exception ex)
            {
                Message.Error(messageDetails: ex.Message);
            }
            finally
            {
                Connection.Close();
            }

            return Message;
        }

        public IEnumerable<SelectListItem> UserTypeSli()
        {
            return repository.GetAll().OrderBy(o => o.UserTypeName).Select(o => new SelectListItem { Value = o.UserTypeId.ToString(), Text = o.UserTypeName }).OrderBy(o => o.Text);
        }
    }
}
