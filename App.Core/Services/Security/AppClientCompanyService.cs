﻿using App.Core.Models;
using App.Core.Models.Security;
using App.Core.Repositories.Security;
using System;
using System.Collections.Generic;
using System.Text;

namespace App.Core.Services.Security
{
    public class AppClientCompanyService : BaseService, IBaseService<AppClientCompany>
    {
        readonly AppClientCompanyRepository repository;

        public AppClientCompanyService()
        {
            repository = new AppClientCompanyRepository(Connection);
        }

        public Message Delete(int id)
        {
            throw new NotImplementedException();
        }

        public AppClientCompany Get(int id)
        {
            return repository.Get(id);
        }

        public IEnumerable<AppClientCompany> GetAll()
        {
            return repository.GetAll();
        }

        public Message Insert(AppClientCompany entity)
        {
            try
            {
                entity.CD = entity.MD = DateTime.Now;
                entity.CompanyId = repository.Insert(entity);
                Message.Success();
            }
            catch(Exception ex)
            {
                Message.Error(messageDetails: ex.Message);
            }
            finally
            {
                Connection.Close();
            }

            return Message;
        }

        public Message Update(AppClientCompany entity)
        {
            try
            {
                entity.MD = DateTime.Now;
                repository.Update(entity);
                Message.Success();
            }
            catch (Exception ex)
            {
                Message.Error(messageDetails: ex.Message);
            }
            finally
            {
                Connection.Close();
            }

            return Message;
        }
    }
}
