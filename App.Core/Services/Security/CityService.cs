﻿using App.Core.Models;
using App.Core.Models.Security;
using App.Core.Repositories.Security;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace App.Core.Services.Security
{
    public class CityService : BaseService, IBaseService<City>
    {
        readonly CityRepository repository;

        public CityService()
        {
            repository = new CityRepository(Connection);
        }

        public Message Delete(int id)
        {
            throw new NotImplementedException();
        }

        public City Get(int id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<City> GetAll()
        {
            throw new NotImplementedException();
        }

        public Message Insert(City entity)
        {
            throw new NotImplementedException();
        }

        public Message Update(City entity)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<SelectListItem> CitySli(int? stateId = null)
        {

            try
            {
                return repository.CitySli(stateId);
            }
            catch (Exception ex)
            {
                throw new Exception("Exception occurred, please contact with system admin.");
            }
            finally
            {
                Connection.Close();
            }
        }
    }
}
