﻿using App.Core.Models;
using App.Core.Models.Security;
using App.Core.Repositories.Security;
using System;
using System.Collections.Generic;
using System.Text;

namespace App.Core.Services.Security
{
    public class ScreenService : BaseService, IBaseService<Screen>
    {
        readonly ScreenRepository repository;

        public ScreenService()
        {
            repository = new ScreenRepository(Connection);
        }

        public Message Delete(int id)
        {
            throw new NotImplementedException();
        }

        public Screen Get(int id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Screen> GetAll()
        {
            throw new NotImplementedException();
        }

        public Message Insert(Screen entity)
        {
            throw new NotImplementedException();
        }

        public Message Update(Screen entity)
        {
            throw new NotImplementedException();
        }
    }
}
