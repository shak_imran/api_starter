﻿
using App.Core.Models;
using App.Core.Models.Security;
using App.Core.Repositories.Security;
using System.Collections.Generic;
using System.Text;

namespace App.Core.Services.Security
{
    public class TimeZoneService : BaseService, IBaseService<TimeZone>
    {
        readonly TimeZoneRepository repository;

        public TimeZoneService()
        {
            repository = new TimeZoneRepository(Connection);
        }

        public Message Delete(int id)
        {
            throw new System.NotImplementedException();
        }

        public TimeZone Get(int id)
        {
            throw new System.NotImplementedException();
        }

        public IEnumerable<TimeZone> GetAll()
        {
            throw new System.NotImplementedException();
        }

        public Message Insert(TimeZone entity)
        {
            throw new System.NotImplementedException();
        }

        public Message Update(TimeZone entity)
        {
            throw new System.NotImplementedException();
        }
    }
}
