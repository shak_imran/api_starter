﻿using App.Core.Models;
using App.Core.Models.Security;
using App.Core.Repositories.Security;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace App.Core.Services.Security
{
    public class CurrencyService : BaseService, IBaseService<Currency>
    {
        readonly CurrencyRepository repository;

        public CurrencyService()
        {
            repository = new CurrencyRepository(Connection);
        }

        public Message Delete(int id)
        {
            throw new NotImplementedException();
        }

        public Currency Get(int id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Currency> GetAll()
        {
            throw new NotImplementedException();
        }

        public Message Insert(Currency entity)
        {
            throw new NotImplementedException();
        }

        public Message Update(Currency entity)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<SelectListItem> CurrencySli()
        {
            return repository.GetAll().OrderBy(o => o.CurrencyName).Select(o => new SelectListItem { Value = o.CurrencyId.ToString(), Text = o.CurrencyName }).OrderBy(o => o.Text);
        }
    }
}
