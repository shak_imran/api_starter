﻿using App.Core.Models;
using App.Core.Models.Security;
using App.Core.Repositories.Security;
using System;
using System.Collections.Generic;
using System.Text;

namespace App.Core.Services.Security
{
    public class SectionService : BaseService, IBaseService<Section>
    {
        readonly SectionRepository repository;

        public SectionService()
        {
            repository = new SectionRepository(Connection);
        }

        public Message Delete(int id)
        {
            throw new NotImplementedException();
        }

        public Section Get(int id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Section> GetAll()
        {
            throw new NotImplementedException();
        }

        public Message Insert(Section entity)
        {
            throw new NotImplementedException();
        }

        public Message Update(Section entity)
        {
            throw new NotImplementedException();
        }
    }
}
