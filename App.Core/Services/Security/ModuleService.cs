﻿using App.Core.Models;
using App.Core.Models.Security;
using App.Core.Repositories.Security;
using System;
using System.Collections.Generic;
using System.Text;

namespace App.Core.Services.Security
{
    public class ModuleService : BaseService, IBaseService<Module>
    {
        readonly ModuleRepository repository;

        public ModuleService()
        {
            repository = new ModuleRepository(Connection);
        }

        public Message Delete(int id)
        {
            throw new NotImplementedException();
        }

        public Module Get(int id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Module> GetAll()
        {
            throw new NotImplementedException();
        }

        public Message Insert(Module entity)
        {
            throw new NotImplementedException();
        }

        public Message Update(Module entity)
        {
            throw new NotImplementedException();
        }
    }
}
