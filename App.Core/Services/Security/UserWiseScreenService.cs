﻿using App.Core.Models;
using App.Core.Models.Security;
using App.Core.Repositories.Security;
using System;
using System.Collections.Generic;
using System.Text;

namespace App.Core.Services.Security
{
    public class UserWiseScreenService : BaseService, IBaseService<UserWiseScreen>
    {
        readonly UserWiseScreenRepository repository;

        public UserWiseScreenService()
        {
            repository = new UserWiseScreenRepository(Connection);
        }

        public Message Delete(int id)
        {
            throw new NotImplementedException();
        }

        public UserWiseScreen Get(int id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<UserWiseScreen> GetAll()
        {
            throw new NotImplementedException();
        }

        public Message Insert(UserWiseScreen entity)
        {
            throw new NotImplementedException();
        }

        public Message Update(UserWiseScreen entity)
        {
            throw new NotImplementedException();
        }
    }
}
