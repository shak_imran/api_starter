﻿using App.Core.Models;
using App.Core.Models.Security;
using App.Core.Repositories.Security;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Text;

namespace App.Core.Services.Security
{
    public class UserCategoryService : BaseService, IBaseService<UserCategory>
    {
        readonly UserCategoryRepository repository;

        public UserCategoryService()
        {
            repository = new UserCategoryRepository(Connection);
        }

        public Message Delete(int id)
        {
            throw new NotImplementedException();
        }

        public UserCategory Get(int id)
        {
            return repository.Get(id);
        }

        public IEnumerable<UserCategory> GetAll()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<SelectListItem> UserCategorySli(int companyId)
        {
            return repository.UserCategorySli(companyId);
        }

        public IEnumerable<UserCategory> GetAll(int companyId)
        {
            return repository.GetAll(companyId);
        }

        public Message Insert(UserCategory entity)
        {
            try
            {
                entity.CD = entity.MD = DateTime.Now;
                entity.UserCategoryId = repository.Insert(entity);
                Message.Success();
            }
            catch (Exception ex)
            {
                Message.Error(messageDetails: ex.Message);
            }
            finally
            {
                Connection.Close();
            }
            return Message;
        }

        public Message Update(UserCategory entity)
        {
            try
            {
                entity.MD = DateTime.Now;
                repository.Update(entity);
                Message.Success();
            }
            catch (Exception ex)
            {
                Message.Error(messageDetails: ex.Message);
            }
            finally
            {
                Connection.Close();
            }
            return Message;
        }
    }
}
