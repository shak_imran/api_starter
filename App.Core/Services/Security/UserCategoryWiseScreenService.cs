﻿using App.Core.Models;
using App.Core.Models.Security;
using App.Core.Repositories.Security;
using System;
using System.Collections.Generic;
using System.Text;

namespace App.Core.Services.Security
{
    public class UserCategoryWiseScreenService : BaseService, IBaseService<UserCategoryWiseScreen>
    {
        readonly UserCategoryWiseScreenRepository repository;

        public UserCategoryWiseScreenService()
        {
            repository = new UserCategoryWiseScreenRepository(Connection);
        }

        public Message Delete(int id)
        {
            throw new NotImplementedException();
        }

        public UserCategoryWiseScreen Get(int id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<UserCategoryWiseScreen> GetAll()
        {
            throw new NotImplementedException();
        }

        public Message Insert(UserCategoryWiseScreen entity)
        {
            throw new NotImplementedException();
        }

        public Message Update(UserCategoryWiseScreen entity)
        {
            throw new NotImplementedException();
        }
    }
}
