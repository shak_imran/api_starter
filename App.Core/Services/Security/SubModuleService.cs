﻿using App.Core.Models;
using App.Core.Models.Security;
using App.Core.Repositories.Security;
using System;
using System.Collections.Generic;
using System.Text;

namespace App.Core.Services.Security
{
    public class SubModuleService : BaseService, IBaseService<SubModule>
    {
        readonly SubModuleRepository repository;

        public SubModuleService()
        {
            repository = new SubModuleRepository(Connection);
        }

        public Message Delete(int id)
        {
            throw new NotImplementedException();
        }

        public SubModule Get(int id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<SubModule> GetAll()
        {
            throw new NotImplementedException();
        }

        public Message Insert(SubModule entity)
        {
            throw new NotImplementedException();
        }

        public Message Update(SubModule entity)
        {
            throw new NotImplementedException();
        }
    }
}
