﻿using App.Core.Models;
using App.Core.Models.Security;
using App.Core.Repositories.Security;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace App.Core.Services.Security
{
    public class StateService : BaseService, IBaseService<State>
    {
        readonly StateRepository repository; 

        public StateService()
        {
            repository = new StateRepository(Connection);
        }

        public Message Delete(int id)
        {
            throw new NotImplementedException();
        }

        public State Get(int id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<State> GetAll()
        {
            throw new NotImplementedException();
        }

        public Message Insert(State entity)
        {
            throw new NotImplementedException();
        }

        public Message Update(State entity)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<SelectListItem> StateSli()
        {
            return repository.GetAll().OrderBy(o => o.Name).Select(o => new SelectListItem { Value = o.StateId.ToString(), Text = o.Name }).OrderBy(o => o.Text);
        }
    }
}
