﻿using App.Core.Models;
using App.Core.Models.Security;
using App.Core.Repositories.Security;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace App.Core.Services.Security
{
    public class AppClientService : BaseService, IBaseService<AppClient>
    {
        readonly AppClientRepository repository;

        public AppClientService()
        {
            repository = new AppClientRepository(Connection);
        }

        public Message Delete(int id)
        {
            throw new NotImplementedException();
        }

        public AppClient Get(int id)
        {
            return repository.Get(id);
        }

        public IEnumerable<AppClient> GetAll()
        {
            return repository.GetAll();
        }

        public Message Insert(AppClient entity)
        {
            try
            {
                entity.CD = entity.MD = DateTime.Now;
                entity.AppClientId = repository.Insert(entity);
                Message.Success();
            }
            catch(Exception ex)
            {
                Message.Error(messageDetails: ex.Message);
            }
            finally
            {
                Connection.Close();
            }

            return Message;
        }

        public Message Update(AppClient entity)
        {
            try
            {
                entity.MD = DateTime.Now;
                repository.Update(entity);
                Message.Success();
            }
            catch (Exception ex)
            {
                Message.Error(messageDetails: ex.Message);
            }
            finally
            {
                Connection.Close();
            }

            return Message;
        }

        public IEnumerable<SelectListItem> AppClientSli()
        {
            return repository.GetAll().OrderBy(o => o.AppClientName).Select(o => new SelectListItem { Value = o.AppClientId.ToString(), Text = o.AppClientName }).OrderBy(o => o.Text);
        }
    }
}
