﻿using App.Core.Models;
using App.Core.Models.Security;
using App.Core.Repositories.Security;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace App.Core.Services.Security
{
    public class CountryService : BaseService, IBaseService<Country>
    {
        readonly CountryRepository repository;

        public CountryService()
        {
            repository = new CountryRepository(Connection);
        }

        public Message Delete(int id)
        {
            throw new NotImplementedException();
        }

        public Country Get(int id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Country> GetAll()
        {
            throw new NotImplementedException();
        }

        public Message Insert(Country entity)
        {
            throw new NotImplementedException();
        }

        public Message Update(Country entity)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<SelectListItem> CountrySli()
        {
            return repository.GetAll().OrderBy(o => o.Name).Select(o => new SelectListItem { Value = o.CountryId.ToString(), Text = o.Name }).OrderBy(o => o.Text);
        }
    }
}
