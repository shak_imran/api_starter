﻿using App.Core.Models;
using App.Core.Models.Security;
using App.Core.Repositories.Security;
using App.Core.Utilities;
using System;
using System.Collections.Generic;
using System.Text;

namespace App.Core.Services.Security
{
    public class UserService : BaseService, IBaseService<User>
    {
        readonly UserRepository repository;

        public UserService()
        {
            repository = new UserRepository(Connection);
        }

        public Message Delete(int id)
        {
            throw new NotImplementedException();
        }

        public User Get(int id)
        {
            throw new NotImplementedException();
        }

        public User Get(int userId, int companyId)
        {
            var user = repository.Get(userId, companyId);
            user.Password = string.Empty;
            return user;
        }

        public IEnumerable<Module> Screens(int userId, int userCategoryId)
        {
            return repository.Screens(userId, userCategoryId);
        }

        public User Get(string userName)
        {
            return repository.Get(userName);
        }

        public IEnumerable<User> GetAll()
        {
            throw new NotImplementedException();
        }
        public IEnumerable<User> GetAll(int companyId)
        {
            var users= repository.GetAll(companyId);
            foreach (var u in users)
            {
                u.Password = string.Empty;
            }
            return users;
        }

        public Message Insert(User entity)
        {
            try
            {
                entity.Password = Encription.Encrypt(entity.Password);
                entity.UserId = repository.Insert(entity);
                Message.Success();
            }
            catch (Exception ex)
            {
                Message.Error(messageDetails: ex.Message);
            }
            finally
            {
                Connection.Close();
            }

            return Message;
        }

        public Message Update(User entity)
        {
            try
            {
                repository.Update(entity);
                Message.Success();
            }
            catch (Exception ex)
            {
                Message.Error(messageDetails: ex.Message);
            }
            finally
            {
                Connection.Close();
            }

            return Message;
        }
    }
}
