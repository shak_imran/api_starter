﻿using App.Core.Models.Security;
using Microsoft.AspNetCore.Mvc.Rendering;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using Dapper;

namespace App.Core.Repositories.Security
{
    public class UserCategoryRepository : BaseRepository<UserCategory>
    {
        public UserCategoryRepository(MySqlConnection connection) : base(connection)
        {
        }

        public IEnumerable<SelectListItem> UserCategorySli(int companyId)
        {
            var q = @"select uc.UserCategoryId Value, uc.UserCategoryName Text FROM usercategory uc WHERE uc.CompanyId is NULL OR uc.CompanyId=@CompanyId ORDER BY uc.UserCategoryName";
            return Connection.Query<SelectListItem>(q, new { CompanyId = companyId });
        }

        public override IEnumerable<UserCategory> GetAll(int companyId)
        {
            var q = @"select * from usercategory uc where uc.CompanyId is NULL OR uc.CompanyId=@CompanyId ORDER BY uc.UserCategoryName";
            return Connection.Query<UserCategory>(q, new { CompanyId = companyId }); 
        }
    }
}
