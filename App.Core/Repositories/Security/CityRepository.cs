﻿using App.Core.Models.Security;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using Dapper;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace App.Core.Repositories.Security
{
    public class CityRepository : BaseRepository<City>
    {
        public CityRepository(MySqlConnection connection) : base(connection)
        {
        }


        public IEnumerable<SelectListItem> CitySli(int? countryId = null)
        {
            var q = "select CityId Value, Name Text FROM city WHERE (@CountyId is NULL OR CountyId=@CountyId)";
            return Connection.Query<SelectListItem>(q, new { CountyId = countryId });
        }
    }
}
