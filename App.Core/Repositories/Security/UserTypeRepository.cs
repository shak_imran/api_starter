﻿using App.Core.Models.Security;
using Dapper;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;

namespace App.Core.Repositories.Security
{
    public class UserTypeRepository : BaseRepository<UserType>
    {
        public UserTypeRepository(MySqlConnection connection) : base(connection)
        {
        }

        public override IEnumerable<UserType> GetAll(int companyId)
        {
            var q = @"select * from usertype uc where uc.CompanyId is NULL OR uc.CompanyId=@CompanyId ORDER BY uc.UserCategoryName";

            return Connection.Query<UserType>(q, new { CompanyId = companyId });
        }
    }
}
