﻿using App.Core.Models.Security;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;

namespace App.Core.Repositories.Security
{
    public class AppClientRepository : BaseRepository<AppClient>
    {
        public AppClientRepository(MySqlConnection connection) : base(connection)
        {
        }
    }
}
