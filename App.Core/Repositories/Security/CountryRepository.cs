﻿using App.Core.Models.Security;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;

namespace App.Core.Repositories.Security
{
    public class CountryRepository : BaseRepository<Country>
    {
        public CountryRepository(MySqlConnection connection) : base(connection)
        {
        }
    }
}
