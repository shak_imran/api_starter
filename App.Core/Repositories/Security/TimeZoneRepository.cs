﻿using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using App.Core.Models.Security;
using MySql.Data.MySqlClient;

namespace App.Core.Repositories.Security
{
    public class TimeZoneRepository : BaseRepository<TimeZone>
    {
        public TimeZoneRepository(MySqlConnection connection) : base(connection)
        {
        }
    }
}
