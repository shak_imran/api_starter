﻿using App.Core.Models.Security;
using System.Data.SqlClient;
using Dapper;
using App.Core.Utilities;
using System.Linq;
using MySql.Data.MySqlClient;
using System.Collections;
using System.Collections.Generic;

namespace App.Core.Repositories.Security
{
    public class UserRepository : BaseRepository<User>
    {
        public UserRepository(MySqlConnection connection) : base(connection)
        {
        }

        public User Get(string userName)
        {
            var q = @"select u.*, uc.UserCategoryName, com.DateFormat, com.TimeFormat, tz.OffsetInMinutes
                        From user u 
                        JOIN usercategory uc on u.UserCategoryId=uc.UserCategoryId
                        LEFT JOIN appclientcompany com on u.CompanyId=com.CompanyId
                        LEFT JOIN timezone tz on com.TimeZoneId=tz.TimeZoneId
                    WHERE u.UserName=@UserName";
            return Connection.Query<User>(q, new { UserName = userName }).FirstOrDefault();
        }

        public override IEnumerable<User> GetAll(int companyId)
        {
            var q = @"Select u.*, uc.UserCategoryName FROM user u 
                      JOIN usercategory uc on u.UserCategoryId=uc.UserCategoryId
                      WHERE u.CompanyId=@CompanyId";

            return Connection.Query<User>(q, new { CompanyId = companyId });
        }

        public IEnumerable<Module> Screens(int userId, int userCategoryId)
        {
            IEnumerable<Module> modules = new List<Module>();

            var data = Connection.QueryMultiple("call SP_GetMenus(@UserId,@UserCategoryId);", new { UserId = userId, UserCategoryId = userCategoryId });

            modules = data.Read<Module>();
            if (modules != null)
            {
                var subModules = data.Read<SubModule>();
                var sections = data.Read<Section>();
                var screens = data.Read<Screen>();

                foreach (var m in modules)
                {
                    var _sm = subModules.Where(o => o.ModuleId == m.ModuleId);

                    foreach (var sm in _sm)
                    {
                        var _s = sections.Where(o => o.SubModuleId == sm.SubModuleId);
                        foreach (var s in _s)
                        {
                            s.Screens = screens.Where(o => o.SectionId == s.SectionId);
                        }
                        sm.Sections = _s;
                    }
                    m.SubModules = _sm;
                }

            }


            return modules;
        }

        public override void Update(User entity)
        {
            var q = QB<User>.UpdateOnly(o => o.Email == o.Email && o.IsActive == o.IsActive && o.UserCategoryId == o.UserCategoryId && o.MD == o.MD && o.MB == o.MB, w => w.UserId == w.UserId && w.CompanyId == w.CompanyId);

            Connection.Execute(q, entity);
        }


    }
}
