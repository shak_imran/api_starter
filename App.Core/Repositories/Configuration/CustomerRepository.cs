﻿using App.Core.Models.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using MySql.Data.MySqlClient;
using Dapper;
using System.Linq;

namespace App.Core.Repositories.Configuration
{
    public class CustomerRepository : BaseRepository<Customer>
    {
        public CustomerRepository(MySqlConnection connection, MySqlTransaction transaction = null) : base(connection, transaction)
        {
        }

        public Customer GetByUserId(int userId)
        {
            var q = @"select * FROM customer WHERE UserId=@UserId;";
            return Connection.Query<Customer>(q, new { UserId = userId }).FirstOrDefault();
        }

        public Customer GetByQuote(int quoteId)
        {
            var q = @"select a.* FROM customer a
                    join customerquote b on a.CustomerId = b.CustomerId Where b.QuoteId = @QouteId";
            return Connection.Query<Customer>(q, new { QouteId = quoteId }).FirstOrDefault();
        }

        public Customer GetByEmail(string email)
        {
            var q = @"select *from customer where Email=@Email;";
            return Connection.Query<Customer>(q, new { Email = email }).FirstOrDefault();
        }

        public Customer GetFull(int customerId)
        {

            var q = @"select *FROM customer WHERE CustomerId=@CustomerId;
                     select *FROM customerfamily WHERE CustomerId=@CustomerId;";

            var data = Connection.QueryMultiple(q, new { CustomerId = customerId });

            var customer = data.Read<Customer>().FirstOrDefault();

            return customer;
        }

        public void SaveImageName(int customerId, string imageName)
        {
            var q = "Update customer set ImageName=@ImageName Where CustomerId=@CustomerId";
            Connection.Execute(q, new { CustomerId = customerId, ImageName = imageName });
        }

        public void SavePersonalDetails(Customer customer)
        {
            var q = @"update customer SET FirstName=@FirstName, LastName=@LastName, DOB=@DOB, MobileNo=@MobileNo, EmergencyMobileNo=@EmergencyMobileNo, PIN=@PIN, NID=@NID, MD=@MD, MB=@MB WHERE CustomerId=@CustomerId";
            Connection.Execute(q, customer);
        }

        public void SavePersonalAddress(Customer customer)
        {
            var q = @"update customer SET Pre_StateId=@Pre_StateId, Pre_CityId=@Pre_CityId, Pre_Address=@Pre_Address, Pre_ZipCode=@Pre_ZipCode, Per_StateId=@Per_StateId, Per_CityId=@Per_CityId, Per_Address=@Per_Address, Per_ZipCode=@Per_ZipCode, MD=@MD, MB=@MB WHERE CustomerId=@CustomerId";
            Connection.Execute(q, customer);
        }

      


        public (int numberOfQuote, int numberOfCompany) NumOfQuoteAndFCompany(int customerId)
        {

            var q = @"select COUNT(*) FROM customerquote WHERE CustomerId=@CustomerId;
                        select COUNT(*) FROM favouritecompany WHERE CustomerId=@CustomerId;";

            var data = Connection.QueryMultiple(q, new { CustomerId = customerId });

            var numberOfQuote = data.Read<int>().FirstOrDefault();
            var numberOfCompany = data.Read<int>().FirstOrDefault();

            return (numberOfQuote, numberOfCompany);
        }
    }
}
