﻿using App.Core.Utilities;
using System;
using System.Collections.Generic;
using Dapper;
using System.Linq;
using MySql.Data.MySqlClient;

namespace App.Core.Repositories
{

    public abstract class BaseRepository<TEntity> : CoreRepository where TEntity : class
    {
        public BaseRepository(MySqlConnection connection,MySqlTransaction transaction = null) : base(connection, transaction)
        {

        }

        public virtual IEnumerable<TEntity> GetAll()
        {
            string query = QB<TEntity>.Select();
            return Connection.Query<TEntity>(query);
        }

        public virtual IEnumerable<TEntity> GetAll(int companyId)
        {
            string query = $"{QB<TEntity>.Select()}  WHERE CompanyId=@CompanyId;";
            return Connection.Query<TEntity>(query,new { CompanyId =companyId});
        }

        public virtual TEntity Get(int id)
        {
            string query = QB<TEntity>.SelectById(id);
            return Connection.Query<TEntity>(query).FirstOrDefault();
        }

        public virtual TEntity Get(int id, int companyId)
        {
            string primaryKey = QB<TEntity>.GetPrimaryKeyColumns().FirstOrDefault();
            string query =$"{QB<TEntity>.Select()} WHERE {primaryKey}=@PKey AND CompanyId=@CompanyId;"  ;
            var p = new DynamicParameters();
            p.Add("@PKey", id);
            p.Add("@CompanyId", companyId);
            return Connection.Query<TEntity>(query, p).FirstOrDefault();
        }

        public virtual int Insert(TEntity entity)
        {
            dynamic id;
            int primaryKeyValue = 0;
            string query = QB<TEntity>.InsertIdentityPKey();
            id = Connection.Query(query, entity,transaction:MSqlTransaction).FirstOrDefault();
            primaryKeyValue = GetPrimaryKeyValue(id);
            return primaryKeyValue;
        }

        public virtual void Update(TEntity entity)
        {
            string query = QB<TEntity>.Update();
            Connection.Execute(query, entity, transaction: MSqlTransaction);
        }

        public virtual void Delete(int id)
        {
            string query = QB<TEntity>.Delete();
            string primaryKey = QB<TEntity>.GetPrimaryKeyColumns().FirstOrDefault();

            var p = new DynamicParameters();
            p.Add("@" +primaryKey, id);
            Connection.Execute(query, p, transaction: MSqlTransaction);
        }

    }


    public class CoreRepository
    {
        public MySqlConnection Connection { get; set; }
        public MySqlTransaction MSqlTransaction { get; set; }

        public CoreRepository(MySqlConnection connection, MySqlTransaction mySqlTransaction=null)
        {
            Connection = connection;
            MSqlTransaction = mySqlTransaction;
        }

        public int GetPrimaryKeyValue(dynamic id)
        {
            if (id == null)
                return 0;
            else
            {
                var pk = 0;
                var firstItem = (IDictionary<string, object>)id;
                foreach (var v in firstItem)
                {
                    pk = Convert.ToInt32(v.Value);
                }
                return pk;
            }
        }

        public void SetTransction(MySqlTransaction mySqlTransaction)
        {
            MSqlTransaction = mySqlTransaction;
        }
    }
}
